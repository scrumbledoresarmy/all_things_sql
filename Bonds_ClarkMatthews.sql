/*Bonds_ClarkMatthews.sql*/

/*1. Show all information about the bond with the CUSIP '28717RH95'.*/
select * from bond where CUSIP = '28717RH95'
select * from rating where rating = 'AA1'

/*2. Show all information about all bonds, in order from shortest (earliest maturity) 
to longest (latest maturity).*/
select * from bond ORDER BY maturity ASC

/*3. Calculate the value of this bond portfolio, as the sum of the product of each 
bond's quantity and price.*/
select sum(quantity*price) from bond

/*4. Show the annual return for each bond, as the product of the quantity and the 
coupon. Note that the coupon rates are quoted as whole percentage points, so to use 
them here you will divide their values by 100.*/
select *, (coupon/100*quantity) AS 'annual return' from bond

/*5. Show bonds only of a certain quality and above, for example those bonds 
with ratings at least AA2. (Don't resort to regular-expression or other string-matching 
tricks; use the bond-rating ordinal.)*/
select distinct rating from rating
select * from rating where rating in ('AA2', 'AA1', 'A3', 'A2', 'A1')
select * from rating where rating <= 'AA2'

/*6. Show the average price and coupon rate for all bonds of each bond rating.*/
select avg(price) as 'average price', avg(coupon), rating as 'average coupon' from bond group by rating

/*7. Calculate the yield for each bond, as the ratio of coupon to price. 
Then, identify bonds that we might consider to be overpriced, as those whose yield is 
less than the expected yield given the rating of the bond.*/
select *, (coupon/price) as 'actual yield' from bond

select b.*, r.expected_yield, convert(decimal(8,4), coupon/price) as 'actual yield' 
from bond b join rating r on b.rating = r.rating 
where (b.coupon/b.price) < r.expected_yield




