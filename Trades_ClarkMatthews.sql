/*Trades_ClarkMatthews.sql*/

/*1. Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. 
This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.*/
select t.* 
from position p join trade t on (p.opening_trade_ID = t.ID or p.closing_trade_ID = t.ID)
where trader_ID = 1 and t.stock = 'AAPL'

/*2. Find the total profit or loss for a given trader over the day, 
as the sum of the product of trade size and price for all sales, 
minus the sum of the product of size and price for all buys.*/
declare @sale int
declare @buys int

select @sale = sum(size*price) --as 'sales' 
from position p join trade t on (p.opening_trade_ID = t.ID and p.opening_trade_ID != p.closing_trade_ID and p.opening_trade_ID is not null)
where buy = 0

select @buys = sum(size*price) --as 'buys' 
from position p join trade t on (p.closing_trade_ID = t.ID and p.closing_trade_ID != p.opening_trade_ID and p.closing_trade_ID is not null)
where buy = 1

declare @dayResults int
select @dayResults = (@sale - @buys)

print CAST(@dayResults AS VARCHAR)

/*3. Develop a view that shows profit or loss for all traders.*/
create view trader_sales as
select
	tr.first_name, tr.last_name,
	sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'profit'
from trade t 
	join position p on p.opening_trade_ID = t.id or p.closing_trade_ID = t.ID
	join trader tr on p.trader_ID = tr.ID
where p.closing_trade_id is not null
	and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name;

